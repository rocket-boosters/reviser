[tool.poetry]
name = "reviser"
version = "0.4.5"
description = "AWS Lambda function/layer version deployment manager."
authors = [
  "Scott Ernst <swernst@gmail.com>",
  "Kevin Schiroo",
  "Rigo Silva"
]
license = "MIT"
readme = "README.md"
homepage = "https://gitlab.com/rocket-boosters/reviser"
repository = "https://gitlab.com/rocket-boosters/reviser"
documentation = "https://gitlab.com/rocket-boosters/reviser"
keywords = ["aws", "lambda"]
classifiers = [
  "Development Status :: 4 - Beta",
  "Environment :: Console",
  "Intended Audience :: Developers",
  "License :: OSI Approved :: MIT License",
  "Programming Language :: Python :: 3.11",
  "Programming Language :: Python :: 3.12",
  "Programming Language :: Python :: 3.13",
  "Topic :: Utilities",
  "Typing :: Typed"
]
exclude = ["reviser/tests"]

[tool.poetry.scripts]
reviser = 'reviser.cli:main'
reviser-shell = 'reviser:main_shell'

[tool.poetry.requires-plugins]
poetry-plugin-export = ">=1.8"

[tool.poetry.dependencies]
python = ">=3.11, <3.14"
boto3 = { version = ">=1.35.88", optional = true }
colorama = { version = ">=0.4.4", optional = true }
Jinja2 = { version = ">=2.11.2", optional = true }
prompt-toolkit = { version = ">=3.0.8", optional = true }
PyYAML = { version = ">=5.3.0", optional = true }
pipper = { version = ">=0.10.0", optional = true }
toml = { version = ">=0.10.0", optional = true }
wheel = { version = ">=0.37.1", optional = true }

[tool.poetry.group.dev.dependencies]
pytest = "*"
black = "*"
coverage = "*"
pytest-cov = "*"
lobotomy = ">=0.3.0"
flake8 = "*"
flake8-black = "*"
mypy = "*"
pydocstyle = ">=6.1.1"
radon = ">=5.0.1"
yamllint = "^1.26.1"
taskipy = ">=1.4.0"

[tool.taskipy.tasks]
# https://github.com/illBeRoy/taskipy
black = "black ."
black_lint = "black . --check"
mypy = "mypy . --install-types --non-interactive --ignore-missing-imports"
flake8 = "flake8 ."
pydocstyle = "pydocstyle ."
yamllint = "yamllint ."
# https://github.com/hadolint/hadolint/releases
hadolint = "hadolint Dockerfile"
radon = "radon cc . && radon mi ."
test = "pytest . --cov-report=term-missing --cov=."
format = "task black"
lint = "task black_lint && task flake8 && task mypy && task pydocstyle && task radon && task yamllint"
check = "task format && task lint && task test"
docs = "python docs_builder.py"
build_images = "python image_builder.py"
images = "python image_builder.py --publish"

[tool.poetry.extras]
shell = [
  "prompt-toolkit",
  "Jinja2",
  "colorama",
  "boto3",
  "PyYAML",
  "pipper",
  "toml",
  "wheel",
]

[build-system]
requires = ["poetry-core>=2.0.0,<3.0.0"]
build-backend = "poetry.core.masonry.api"
